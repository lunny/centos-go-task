# Usage

[![Build Status](https://drone.gitea.com/api/badges/lunny/centos-go-task/status.svg)](https://drone.gitea.com/lunny/centos-go-task)

This assumes that you are using go mod to build.

```shell
docker run --rm -v "$PWD":/myrepo -w /myrepo lunny/centos-go:latest go build
```